*In the name of Allah*

# MadarKharj

### A mobile application for justly calculation of group expends

### 📲 [Download + Description + Web Version](https://mwxgaf.github.io/madarkharj)

***Used tools:***

* JavaScript
	* Apache Cordova
	* Jquery
	* JqueryMobile
	* JSON
	* PersianDate
* CSS
	* JqueryMobile
* HTML

_Made in ***Iran***_ :iran:
